return {
    'rebelot/kanagawa.nvim',
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v3.x',
        lazy = true,
        config = false,
        init = function()
            -- Disable automatic setup, we are doing it manually
            vim.g.lsp_zero_extend_cmp = 0
            vim.g.lsp_zero_extend_lspconfig = 0
        end,
    },
    {
        'williamboman/mason.nvim',
        lazy = false,
        config = function()
            local mason = require("mason")
            mason.setup({
                PATH = "append" -- use "prepend" to give mason installs priority
            })
        end
    },
    {
        'neovim/nvim-lspconfig',
        cmd = { 'LspInfo', 'LspInstall', 'LspStart' },
        event = { 'BufReadPre', 'BufNewFile' },
        dependencies = {
            -- { 'hrsh7th/cmp-nvim-lsp' },
            { 'williamboman/mason-lspconfig.nvim' },
        },
        config = function()
            -- This is where all the LSP shenanigans will live
            local lsp_zero = require('lsp-zero')

            lsp_zero.extend_lspconfig()

            lsp_zero.on_attach(function(client, bufnr)
                -- see :help lsp-zero-keybindings
                -- to learn the available actions
                lsp_zero.default_keymaps({ buffer = bufnr })
            end)

            local util = require('lspconfig/util')
            local path = util.path

            require('mason-lspconfig').setup({
                ensure_installed = { 'lua_ls', 'rust_analyzer', 'pylsp' },
                handlers = {
                    lsp_zero.default_setup,
                    lua_ls = function()
                        -- (Optional) Configure lua language server for neovim
                        -- local lua_opts = lsp_zero.nvim_lua_ls()
                        -- require('lspconfig').lua_ls.setup(lua_opts)
                        require('lspconfig').lua_ls.setup({
                            settings = {
                                Lua = {
                                    diagnostics = {
                                        disable = { "lowercase-global", "undefined-global" }
                                    },
                                    hint = {
                                        enable = false,
                                        arrayIndex = "Auto",
                                        await = true,
                                        paramName = "All",
                                        paramType = true,
                                        semicolon = "SameLine",
                                        setType = false,
                                    },
                                },
                            },
                        })
                    end,

                    pylsp = function()
                        -- if pylsp is inside virtual-env, use that
                        local _pylsp = function()
                            local env = vim.env.VIRTUAL_ENV or vim.env.CONDA_PREFIX
                            if env then
                                local pylsp_path = path.join(env, 'bin', 'pylsp');
                                local  match = vim.fn.glob(pylsp_path)
                                if match ~= ''then
                                    return pylsp_path
                                end
                            end
                            return "pylsp"
                        end

                        require("lspconfig").pylsp.setup({
                            cmd = { _pylsp() },
                            settings = {
                                pylsp = {
                                    plugins = {
                                        ruff = { enabled = true, extendSelect = { "I" } },
                                        pylsp_ruff = { enabled = true },
                                        mypy = { enabled = true },
                                        flake8 = { enabled = false },
                                        black = { enabled = false },
                                        pylsp_mypy = { enabled = true },
                                        pylsp_isort = { enabled = true },
                                        isort = { enabled = false },
                                        autopep8 = { enabled = false },
                                        mccabe = { enabled = false },
                                        pycodestyle = { enabled = false },
                                        pyflakes = { enabled = false },
                                        pylint = { enabled = false },
                                        yapf = { enabled = false },
                                    },
                                },
                            },
                        })
                    end
                }
            })
        end
    },
    {
        'nvim-treesitter/nvim-treesitter',
        config = function()
            require("nvim-treesitter.configs").setup({
                incremental_selection = {
                    enable = true,
                    keymaps = {
                        node_incremental = "v",
                        node_decremental = "V",
                    },
                }
            })
        end
    },
}
