local keymap = vim.keymap.set
local MiniAI = require('mini.ai')
local MiniPick = require('mini.pick')
local MiniFiles = require('mini.files')
local MiniSessions = require('mini.sessions')
local MiniExtra = require('mini.extra')


-- MiniPick Colorscheme Picker
local set_colorscheme = function(name) pcall(vim.cmd, 'colorscheme ' .. name) end
local pick_colorscheme = function()
    local init_scheme = vim.g.colors_name
    local new_scheme = MiniPick.start({
        source = {
            items = vim.fn.getcompletion("", "color"),
            preview = function(_, item)
                set_colorscheme(item)
            end,
            choose = set_colorscheme
        },
        mappings = {
            preview = {
                char = '<C-p>',
                func = function()
                    local item = MiniPick.get_picker_matches()
                    pcall(vim.cmd, 'colorscheme ' .. item.current)
                end
            }
        }
    })
    if new_scheme == nil then set_colorscheme(init_scheme) end
end

-- General Vim Things
keymap("n", "<leader>wq", "<cmd>wqa<cr>", { noremap = true, silent = true, desc = 'Quit' })
keymap("n", "<leader>ul", "<cmd>Lazy<cr>", { noremap = true, silent = true, desc = 'Lazy' })

-- Finding Stuff
keymap("n", "<leader>ff", function() MiniPick.builtin.files({tool='git'}) end, { noremap = true, silent = true, desc = 'Find Git File' })
keymap("n", "<leader>fF", function() MiniPick.builtin.files({tool='fd'}) end, { noremap = true, silent = true, desc = 'Find File' })
keymap("n", "<leader>e", function() MiniFiles.open(vim.api.nvim_buf_get_name(0)) end,
    { noremap = true, silent = true, desc = 'Find Manualy' })
keymap("n", "<leader>fb", function() MiniPick.builtin.buffers() end,
    { noremap = true, silent = true, desc = 'Find Buffer' })
keymap("n", "<leader>fg", function() MiniPick.builtin.grep_live() end,
    { noremap = true, silent = true, desc = 'Find String' })
keymap("n", "<leader>fwg", function()
        local wrd = vim.fn.expand("<cWORD>")
        MiniPick.builtin.grep_live({pattern = wrd})
    end,
    { noremap = true, silent = true, desc = 'Find String Cursor' })
keymap("n", "<leader>fh", function() MiniPick.builtin.help() end, { noremap = true, silent = true, desc = 'Find Help' })
keymap("n", "<leader>fl", function() MiniExtra.pickers.hl_groups() end,
    { noremap = true, silent = true, desc = 'Find HL Groups' })
keymap("n", "<leader>fc", pick_colorscheme, { noremap = true, silent = true, desc = 'Change Colorscheme' })

-- Session Related Keymaps
keymap("n", "<leader>s", function()
    vim.cmd('wa')
    MiniSessions.write()
    MiniSessions.select()
end, { noremap = true, silent = true, desc = 'Switch Session' })
-- keymap("n", "<leader>sw", function() vim.cmd('wa') MiniSessions.write() end, { noremap = true, silent = true, desc = 'Save Session' })

-- Buffer Related Keymaps
keymap("n", "<leader>bd", "<cmd>bd<cr>", { noremap = true, silent = true, desc = 'Close Buffer' })
keymap("n", "<leader>bf", function() vim.lsp.buf.format() end, { noremap = true, silent = true, desc = 'Format Buffer' })
-- keymap("n", "<leader>bf", "gg=G<C-o>", { noremap = true, silent = true, desc = 'Format Buffer' })
keymap("n", "<leader>bl", "<cmd>bnext<cr>", { silent = true, desc = 'Next Buffer' })
keymap("n", "<leader>bh", "<cmd>bprevious<cr>", { silent = true, desc = 'Previous Buffer' })
keymap("n", "<TAB>", "<C-^>", { noremap = true, silent = true, desc = "Alternate buffers" })

-- Git Related Keymaps
keymap("n", "<leader>gl", "<cmd>terminal gitui<cr>", { noremap = true, silent = true, desc = 'Gitui' })
keymap("n", "<leader>gp", "<cmd>terminal git pull<cr>", { noremap = true, silent = true, desc = 'Git Push' })
keymap("n", "<leader>gs", "<cmd>terminal git push<cr>", { noremap = true, silent = true, desc = 'Git Pull' })
keymap("n", "<leader>ga", "<cmd>terminal git add .<cr>", { noremap = true, silent = true, desc = 'Git Add All' })
keymap("n", "<leader>gc", '<cmd>terminal git commit -m "Autocommit from MVIM"<cr>',
    { noremap = true, silent = true, desc = 'Git Autocommit' })

-- LSP Keymaps
keymap("n", "<leader>ld", function() vim.lsp.buf.definition() end,
    { noremap = true, silent = true, desc = 'Go To Definition' })
keymap("n", "<leader>ls", "<cmd>Pick lsp scope='document_symbol'<cr>",
    { noremap = true, silent = true, desc = 'Show all Symbols' })
keymap("n", "<leader>lr", function() vim.lsp.buf.rename() end, { noremap = true, silent = true, desc = 'Rename This' })
keymap("n", "<leader>la", function() vim.lsp.buf.code_action() end,
    { noremap = true, silent = true, desc = 'Code Actions' })

-- Diagnostics
local diagnostic_goto = function(next, severity)
  local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
  severity = severity and vim.diagnostic.severity[severity] or nil
  return function()
    go({ severity = severity })
  end
end
keymap("n", "<leader>cd", vim.diagnostic.open_float, { desc = "Line Diagnostics" })
keymap("n", "]d", diagnostic_goto(true), { desc = "Next Diagnostic" })
keymap("n", "[d", diagnostic_goto(false), { desc = "Prev Diagnostic" })
keymap("n", "]e", diagnostic_goto(true, "ERROR"), { desc = "Next Error" })
keymap("n", "[e", diagnostic_goto(false, "ERROR"), { desc = "Prev Error" })
keymap("n", "]w", diagnostic_goto(true, "WARN"), { desc = "Next Warning" })
keymap("n", "[w", diagnostic_goto(false, "WARN"), { desc = "Prev Warning" })

-- UI Related Keymaps
-- Window Navigation
keymap("n", "<leader>wl", "<cmd>wincmd l<cr>", { noremap = true, silent = true, desc = 'Focus Left' })
keymap("n", "<leader>wk", "<cmd>wincmd k<cr>", { noremap = true, silent = true, desc = 'Focus Up' })
keymap("n", "<leader>wj", "<cmd>wincmd j<cr>", { noremap = true, silent = true, desc = 'Focus Down' })
keymap("n", "<leader>wh", "<cmd>wincmd h<cr>", { noremap = true, silent = true, desc = 'Focus Right' })
keymap("n", "<leader>wq", "<cmd>wincmd q<cr>", { noremap = true, silent = true, desc = 'Focus Right' })

-- Zen mode
keymap("n", "<leader>uz", "<cmd>ZenMode<cr>", { noremap = true, silent = true, desc = 'Toggle Zen' })


-- nvim-spider
keymap(
	{ "n", "o", "x" },
	"w",
	"<cmd>lua require('spider').motion('w')<CR>",
	{ desc = "Spider-w" }
)
keymap(
	{ "n", "o", "x" },
	"e",
	"<cmd>lua require('spider').motion('e')<CR>",
	{ desc = "Spider-e" }
)
keymap(
	{ "n", "o", "x" },
	"b",
	"<cmd>lua require('spider').motion('b')<CR>",
	{ desc = "Spider-b" }
)
